# exfmt
std::ostreamの書式をちょっと拡張する

## 使い方

```cpp
{
  // std::ostreamをバインドする
  exfmt::out exout{&std::cout};

  // std::ostreamと同じように使用できる
  exout << "text" << 123 << '\n';
  exout << std::hex << std::setfill('0') << std::setw(4) << 255 << '\n';
}
// exfmt::outの寿命が尽きればフラグ等は元に戻る
std::cout << 123 << '\n';

// デフォルトでstd::coutにバインドされる
// 一時オブジェクトを使用してその場限りの書式として利用する
exfmt::out() << std::hex << 255 << '\n';
exfmt::out() << 255 << '\n';
```
![out1](img/out1.png "out1 image")

```cpp
// エスケープシーケンスを使用した書式設定
exfmt::out() << exfmt::bg_yellow << exfmt::fg_red << exfmt::bold << "nice text" << exfmt::endl;

// 書式のショートカット設定
constexpr auto style{exfmt::style<exfmt::fg_green,
                                  exfmt::italic,
                                  exfmt::underline,
                                  exfmt::stdfmt<std::hex>>};
constexpr auto fill{exfmt::fill<'0', 4>};
exfmt::out() << style << "0x" << fill << 0xcafe << exfmt::endl;

// 単発書式の設定
constexpr auto oneshot{exfmt::oneshot<exfmt::bg_magenta,
                                      exfmt::stdfmt<std::dec>,
                                      fill>};
exfmt::out() << style << 255 << oneshot << 255 << 255 << exfmt::endl;
```
![out2](img/out2.png "out2 image")