#pragma once

#include <ctime>
#include <iomanip>
#include <iostream>

namespace exfmt {

class out {
 public:
  using sgrflags = std::uint32_t;
  // clang-format off
  static constexpr sgrflags bold      {1u << 0};
  static constexpr sgrflags faint     {1u << 1};
  static constexpr sgrflags italic    {1u << 2};
  static constexpr sgrflags underline {1u << 3};
  static constexpr sgrflags blink     {1u << 4};
  static constexpr sgrflags inverse   {1u << 5};
  static constexpr sgrflags hidden    {1u << 6};
  static constexpr sgrflags strike    {1u << 7};
  static constexpr sgrflags fg_color  {1u << 8};
  static constexpr sgrflags fg_black  {fg_color | (0u << 9)};
  static constexpr sgrflags fg_red    {fg_color | (1u << 9)};
  static constexpr sgrflags fg_green  {fg_color | (2u << 9)};
  static constexpr sgrflags fg_yellow {fg_color | (3u << 9)};
  static constexpr sgrflags fg_blue   {fg_color | (4u << 9)};
  static constexpr sgrflags fg_magenta{fg_color | (5u << 9)};
  static constexpr sgrflags fg_cyan   {fg_color | (6u << 9)};
  static constexpr sgrflags fg_white  {fg_color | (7u << 9)};
  static constexpr sgrflags bg_color  {fg_color   << 4};
  static constexpr sgrflags bg_black  {fg_black   << 4};
  static constexpr sgrflags bg_red    {fg_red     << 4};
  static constexpr sgrflags bg_green  {fg_green   << 4};
  static constexpr sgrflags bg_yellow {fg_yellow  << 4};
  static constexpr sgrflags bg_blue   {fg_blue    << 4};
  static constexpr sgrflags bg_magenta{fg_magenta << 4};
  static constexpr sgrflags bg_cyan   {fg_cyan    << 4};
  static constexpr sgrflags bg_white  {fg_white   << 4};
  // clang-format on

  out(std::ostream* os = &std::cout)
      : os_{*os},
        fmt_backup_{os_.flags()},
        fill_backup_{os_.fill()},
        sgrbuf_{0},
        sgrstate_{0},
        oneshot_{false, 0, ' ', 0} {}

  ~out() {
    os_.flags(fmt_backup_);
    os_.fill(fill_backup_);
    if (sgrstate_) os_ << "\x1b[m";
  }

  operator std::ostream&() { return os_; }

  friend auto operator<<(out& self, std::ios_base& (*func)(std::ios_base&))
      -> out& {
    self.syncsgr();
    func(self.os_);
    return self;
  }

  friend auto operator<<(out& self, std::ostream& (*func)(std::ostream&))
      -> out& {
    self.syncsgr();
    func(self.os_);
    return self;
  }

  friend auto operator<<(out& self, out& (*func)(out&)) -> out& {
    self.syncsgr();
    func(self);
    return self;
  }

  template <class T>
  friend auto operator<<(out& self, const T& v)
      -> decltype(std::declval<std::ostream&>() << v, std::declval<out&>()) {
    self.syncsgr();
    self.os_ << v;
    if (self.oneshot_.use) {
      self.os_.flags(self.oneshot_.fmt);
      self.os_.fill(self.oneshot_.fill);
      self.sgr(self.oneshot_.sgr);
      self.oneshot_.use = false;
      self.oneshot_.fmt = 0;
      self.oneshot_.fill = ' ';
      self.oneshot_.sgr = 0;
    }
    return self;
  }

  template <class T>
  friend auto operator<<(out&& self, const T& v) -> out&& {
    self << v;
    return std::move(self);
  }

  sgrflags sgr() const { return sgrbuf_; }

  sgrflags sgr(sgrflags sgrfl) { return std::exchange(sgrbuf_, sgrfl); }

  sgrflags setsgr(sgrflags sgrfl, sgrflags mask = 0) {
    auto new_flag{sgrfl | (sgrbuf_ & (~mask))};
    if (sgrfl & fg_color) {
      new_flag = (new_flag & 0b1111'0000'1111'1111u) |
                 (sgrfl & 0b0000'1111'0000'0000u);
    }
    if (sgrfl & bg_color) {
      new_flag = (new_flag & 0b0000'1111'1111'1111u) |
                 (sgrfl & 0b1111'0000'0000'0000u);
    }
    return sgr(new_flag);
  }

  sgrflags unsetsgr(sgrflags mask) { return sgr(sgr() & ~mask); }

  void syncsgr() {
    const auto diff{(sgrbuf_ ^ sgrstate_) & 0b1111'1111'1111'1111u};
    if (diff == 0) return;

    sgrstate_ = sgrbuf_;
    if (sgrstate_ == 0) {
      os_ << "\x1b[m";
      return;
    }

    os_ << "\x1b[";

    bool is_first{true};
    auto delim{[&is_first, this]() mutable {
      is_first ? (is_first = false, 0) : (os_ << ';', 0);
    }};
    auto style{[diff, &delim, this](auto flag, auto set, auto reset) mutable {
      if (diff & flag) {
        delim();
        (sgrstate_ & flag) ? os_ << set : os_ << reset;
      }
    }};

    // clang-format off
    style(bold      , "1", "22");
    style(faint     , "2", "22");
    style(italic    , "3", "23");
    style(underline , "4", "24");
    style(blink     , "5", "25");
    style(inverse   , "7", "27");
    style(hidden    , "8", "28");
    style(strike    , "9", "29");
    // clang-format on

    auto color{[diff, &delim, this](auto flag, auto mask, int shift, const auto& table,
                                    auto reset) mutable {
      if (diff & mask) {
        delim();
        (sgrstate_ & flag) ? os_ << table[(sgrstate_ >> shift) & 0b111]
                           : os_ << reset;
      }
    }};

    constexpr std::string_view fg[]{"30", "31", "32", "33",
                                    "34", "35", "36", "37"};
    color(fg_color, fg_white, 9, fg, "39");

    constexpr std::string_view bg[]{"40", "41", "42", "43",
                                    "44", "45", "46", "47"};
    color(bg_color, bg_white, 13, bg, "49");

    os_ << 'm';
  }

  void oneshot() {
    if (not oneshot_.use) {
      oneshot_.use = true;
      oneshot_.fmt = os_.flags();
      oneshot_.fill = os_.fill();
      oneshot_.sgr = sgr();
    }
  }

 private:
  std::ostream& os_;
  const std::ios_base::fmtflags fmt_backup_;
  const std::ostream::char_type fill_backup_;
  sgrflags sgrbuf_;
  sgrflags sgrstate_;
  struct {
    bool use;
    std::ios_base::fmtflags fmt;
    std::ostream::char_type fill;
    sgrflags sgr;
  } oneshot_;
};

template <std::ios_base& (*... Fc)(std::ios_base&)>
auto inline stdfmt(out& o) -> out& {
  using swallow = std::initializer_list<int>;
  (void)swallow{(void(Fc(static_cast<std::ostream&>(o))), 0)...};
  return o;
}

template <out& (*... Fc)(out&)>
auto inline style(out& o) -> out& {
  using swallow = std::initializer_list<int>;
  (void)swallow{(void(Fc(o)), 0)...};
  return o;
}

template <out& (*... Fc)(out&)>
auto inline oneshot(out& o) -> out& {
  o.oneshot();
  return style<Fc...>(o);
}

auto inline bell(out& o) -> out& {
  static_cast<std::ostream&>(o).put('\a');
  return o;
}

auto inline bs(out& o) -> out& {
  static_cast<std::ostream&>(o).put('\b');
  return o;
}

auto inline tab(out& o) -> out& {
  const auto backup{o.sgr(0)};
  o.syncsgr();
  static_cast<std::ostream&>(o).put('\t');
  o.sgr(backup);
  return o;
}

auto inline lf(out& o) -> out& {
  const auto backup{o.sgr(0)};
  o.syncsgr();
  static_cast<std::ostream&>(o).put('\n');
  o.sgr(backup);
  return o;
}

auto inline vtab(out& o) -> out& {
  const auto backup{o.sgr(0)};
  o.syncsgr();
  static_cast<std::ostream&>(o).put('\v');
  o.sgr(backup);
  return o;
}

auto inline ff(out& o) -> out& {
  const auto backup{o.sgr(0)};
  o.syncsgr();
  static_cast<std::ostream&>(o).put('\f');
  o.sgr(backup);
  return o;
}

auto inline cr(out& o) -> out& {
  static_cast<std::ostream&>(o).put('\r');
  return o;
}

auto inline endl(out& o) -> out& {
  const auto backup{o.sgr(0)};
  o.syncsgr();
  std::endl(static_cast<std::ostream&>(o));
  o.sgr(backup);
  return o;
}

template <char Ch, int W>
auto inline fill(out& o) -> out& {
  o.syncsgr();
  static_cast<std::ostream&>(o).fill(Ch);
  static_cast<std::ostream&>(o).width(W);
  return o;
}

template <const char* Fmt = nullptr>
auto inline now(out& o) -> out& {
  o.syncsgr();
  auto t{std::time(nullptr)};
  const auto* lt{std::localtime(&t)};
  if constexpr (Fmt != nullptr) {
    static_cast<std::ostream&>(o) << std::put_time(lt, Fmt);
  } else {
    static_cast<std::ostream&>(o) << std::put_time(lt, "%c");
  }
  return o;
}

auto inline now8601(out& o) -> out& {
  static constexpr char fmt[]{"%Y%m%dT%H%M%S%z"};
  return now<fmt>(o);
}

template <char... Cs>
auto inline puts(out& o) -> out& {
  constexpr char cs[]{Cs...};
  o.syncsgr();
  static_cast<std::ostream&>(o).write(cs, sizeof...(Cs));
  return o;
}

auto inline bold(out& o) -> out& {
  o.setsgr(out::bold);
  return o;
}

auto inline faint(out& o) -> out& {
  o.setsgr(out::faint);
  return o;
}

auto inline italic(out& o) -> out& {
  o.setsgr(out::italic);
  return o;
}

auto inline underline(out& o) -> out& {
  o.setsgr(out::underline);
  return o;
}

auto inline blink(out& o) -> out& {
  o.setsgr(out::blink);
  return o;
}

auto inline inverse(out& o) -> out& {
  o.setsgr(out::inverse);
  return o;
}

auto inline hidden(out& o) -> out& {
  o.setsgr(out::hidden);
  return o;
}

auto inline strike(out& o) -> out& {
  o.setsgr(out::strike);
  return o;
}

auto inline fg_black(out& o) -> out& {
  o.setsgr(out::fg_black);
  return o;
}

auto inline fg_red(out& o) -> out& {
  o.setsgr(out::fg_red);
  return o;
}

auto inline fg_green(out& o) -> out& {
  o.setsgr(out::fg_green);
  return o;
}

auto inline fg_yellow(out& o) -> out& {
  o.setsgr(out::fg_yellow);
  return o;
}

auto inline fg_blue(out& o) -> out& {
  o.setsgr(out::fg_blue);
  return o;
}

auto inline fg_magenta(out& o) -> out& {
  o.setsgr(out::fg_magenta);
  return o;
}

auto inline fg_cyan(out& o) -> out& {
  o.setsgr(out::fg_cyan);
  return o;
}

auto inline fg_white(out& o) -> out& {
  o.setsgr(out::fg_white);
  return o;
}

auto inline fg_default(out& o) -> out& {
  o.unsetsgr(out::fg_color);
  return o;
}

auto inline bg_black(out& o) -> out& {
  o.setsgr(out::bg_black);
  return o;
}

auto inline bg_red(out& o) -> out& {
  o.setsgr(out::bg_red);
  return o;
}

auto inline bg_green(out& o) -> out& {
  o.setsgr(out::bg_green);
  return o;
}

auto inline bg_yellow(out& o) -> out& {
  o.setsgr(out::bg_yellow);
  return o;
}

auto inline bg_blue(out& o) -> out& {
  o.setsgr(out::bg_blue);
  return o;
}

auto inline bg_magenta(out& o) -> out& {
  o.setsgr(out::bg_magenta);
  return o;
}

auto inline bg_cyan(out& o) -> out& {
  o.setsgr(out::bg_cyan);
  return o;
}

auto inline bg_white(out& o) -> out& {
  o.setsgr(out::bg_white);
  return o;
}

auto inline bg_default(out& o) -> out& {
  o.unsetsgr(out::bg_color);
  return o;
}

namespace detail {

class rewrite {
 public:
  rewrite(int line): line_{line} {}

  friend auto operator<<(out& o, const rewrite& self) -> out& {
    if (self.line_ < 0) return o;

    const auto backup{o.sgr(0)};
    o.syncsgr();

    auto& os{static_cast<std::ostream&>(o)};
    os.put('\r');
    for (int line = 0; line < self.line_; ++line) {
      os.write("\x1bM", 2);
    }
    os.write("\x1b[J", 3);

    o.sgr(backup);
    return o;
  }

 private:
  int line_;
};

}

auto inline rewrite(int line) -> detail::rewrite {
  return detail::rewrite{line};
}

template <int Line>
auto rewrite(out& o) -> out& {
  return o << detail::rewrite{Line};
}

}  // namespace exfmt