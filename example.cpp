#include <sstream>

#include "exfmt.hpp"

void check(int line, const std::string_view& expected,
           const std::string_view& actual);

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) {
  // exfmt::outの寿命が尽きるときにフォーマットフラグを元に戻す
  {
    std::ostringstream oss;
    const auto flags{oss.flags()};
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::stdfmt<std::hex> << 255;

      check(__LINE__, "ff", oss.str());
      assert(flags != oss.flags());
      assert(oss.flags() ==
             ((flags & ~std::ios_base::basefield) | std::ios_base::hex));
    }
    assert(flags == oss.flags());
    assert(oss.flags() !=
           ((flags & ~std::ios_base::basefield) | std::ios_base::hex));
  }

  // exfmt::outの寿命が尽きるときに埋め文字を元に戻す
  {
    std::ostringstream oss;
    const auto fill{oss.fill()};
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::fill<'?', 8> << "abc";

      check(__LINE__, "?????abc", oss.str());
      assert(fill != oss.fill());
      assert(oss.fill() == '?');
    }
    assert(fill == oss.fill());
    assert(oss.fill() != '?');
  }

  // エスケープシーケンスで書式を設定する
  // exfmt::outの寿命が尽きるときに書式をデフォルトに戻す
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::bold << "bold";

      check(__LINE__, "\x1b[1mbold", oss.str());
    }
    check(__LINE__, "\x1b[1mbold\x1b[m", oss.str());
  }

  // 複数の書式の組み合わせを定義して使用する
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      constexpr auto styles{
          exfmt::style<exfmt::bold, exfmt::faint, exfmt::italic,
                       exfmt::underline, exfmt::blink, exfmt::inverse,
                       exfmt::hidden, exfmt::strike>};
      ossex << styles << "styles";
    }
    check(__LINE__, "\x1b[1;2;3;4;5;7;8;9mstyles\x1b[m", oss.str());
  }

  // 文字色の変更
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::fg_black << "black";
      ossex << exfmt::fg_red << "red";
      ossex << exfmt::fg_green << "green";
      ossex << exfmt::fg_yellow << "yellow";
      ossex << exfmt::fg_blue << "blue";
      ossex << exfmt::fg_magenta << "magenta";
      ossex << exfmt::fg_cyan << "cyan";
      ossex << exfmt::fg_white << "white";
      ossex << exfmt::fg_default << "default";
    }
    check(__LINE__,
          "\x1b[30mblack\x1b[31mred\x1b[32mgreen\x1b[33myellow\x1b[34mblue"
          "\x1b[35mmagenta\x1b[36mcyan\x1b[37mwhite\x1b[39mdefault\x1b[m",
          oss.str());
  }

  // 背景色の変更
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::bg_black << "black";
      ossex << exfmt::bg_red << "red";
      ossex << exfmt::bg_green << "green";
      ossex << exfmt::bg_yellow << "yellow";
      ossex << exfmt::bg_blue << "blue";
      ossex << exfmt::bg_magenta << "magenta";
      ossex << exfmt::bg_cyan << "cyan";
      ossex << exfmt::bg_white << "white";
      ossex << exfmt::bg_default << "default";
    }
    check(__LINE__,
          "\x1b[40mblack\x1b[41mred\x1b[42mgreen\x1b[43myellow\x1b[44mblue"
          "\x1b[45mmagenta\x1b[46mcyan\x1b[47mwhite\x1b[49mdefault\x1b[m",
          oss.str());
  }

  // 直後の出力にのみ書式を設定する
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      constexpr auto oneshot{
          exfmt::oneshot<exfmt::fg_green, exfmt::stdfmt<std::hex>,
                         exfmt::fill<'0', 4>>};
      ossex << exfmt::fg_red << 255 << oneshot << 255 << 255;
    }
    check(__LINE__, "\x1b[31m255\x1b[32m00ff\x1b[31m255\x1b[m", oss.str());
  }

  // 背景色を変更した場合、改行時に出力が意図しない結果になるかもしれない
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::bg_red;
      ossex << "line1" << '\n';
      ossex << "line2" << '\n';
      ossex << "line3" << std::endl;
    }
    check(__LINE__, "\x1b[41mline1\nline2\nline3\n\x1b[m", oss.str());
  }

  // 背景色を変更した場合でもきれいに改行する
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      ossex << exfmt::bg_red;
      ossex << "line1" << exfmt::lf;
      ossex << "line2" << exfmt::lf;
      ossex << "line3" << exfmt::endl;
    }
    check(__LINE__,
          "\x1b[41mline1\x1b[m\n"
          "\x1b[41mline2\x1b[m\n"
          "\x1b[41mline3\x1b[m\n",
          oss.str());
  }

  // 出力を消して書き直す
  {
    std::ostringstream oss;
    {
      exfmt::out ossex{&oss};

      ossex << "line1" << exfmt::lf;
      ossex << "line2";
      ossex << exfmt::rewrite<0>;
      ossex << "line3(erase line2)" << exfmt::lf;
      ossex << "line4" << exfmt::lf;
      ossex << "line5" << exfmt::lf;
      ossex << "line6";
      ossex << exfmt::rewrite<2>;
      ossex << "line7(erase line6, 5, 4)";
    }
    check(__LINE__,
          "line1\nline2\r\x1b[J"
          "line3(erase line2)\nline4\nline5\nline6\r\x1bM\x1bM\x1b[J"
          "line7(erase line6, 5, 4)",
          oss.str());
  }

  // 一時オブジェクトに書き込む
  // デフォルトでstd::coutに紐づく
  // 現在時間を出力する
  {
    exfmt::out() << exfmt::now<> << exfmt::lf;

    static constexpr char format[]{"[%H:%M:%S]"};
    exfmt::out() << exfmt::now<format> << exfmt::lf;

    exfmt::out() << exfmt::now8601 << exfmt::endl;
  }
}

void check(int line, const std::string_view& expected,
           const std::string_view& actual) {
  constexpr auto print_seq([](const std::string_view& seq) {
    for (const auto c : seq) {
      if (c < ' ') {
        constexpr char table[' ']{'0', '?', '?', '?', '?', '?', '?', 'a',
                                  'b', 't', 'n', 'v', 'f', 'r', '?', '?',
                                  '?', '?', '?', '?', '?', '?', '?', '?',
                                  '?', '?', '?', 'e', '?', '?', '?', '?'};
        std::cout.put('\\');
        std::cout.put(table[static_cast<int>(c)]);
      } else {
        std::cout.put(c);
      }
    }
  });
  std::cout << "line: " << line << "\ncheck\n  expected: ";
  print_seq(expected);
  std::cout << "\n  actual  : ";
  print_seq(actual);
  std::cout << '\n';
  std::cout << actual << '\n' << std::endl;
  assert(expected == actual);
}